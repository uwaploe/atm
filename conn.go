package atm

import (
	"context"
	"fmt"
	"time"
)

// Conn provides a generic stream connection to a remote modem
type Conn interface {
	// Read reads data from the remote modem
	Read(b []byte) (n int, err error)
	// Write write data to the remote modem
	Write(b []byte) (n int, err error)
	// Close closes the connection
	Close() error
	// RemoteAddr returns the address of the remote modem
	RemoteAddr() int
	// WakeupRemote sends a wakeup command to the remote modem
	WakeupRemote(ctx context.Context) error
}

type remoteConn struct {
	dev        *Device
	remoteAddr int
	baud       int
	closed     bool
}

func (c *remoteConn) RemoteAddr() int {
	return c.remoteAddr
}

func (c *remoteConn) Read(p []byte) (int, error) {
	if c.closed {
		return 0, ErrClosed
	}
	return c.dev.port.Read(p)
}

func (c *remoteConn) Write(p []byte) (int, error) {
	if c.closed {
		return 0, ErrClosed
	}
	return c.dev.port.Write(p)
}

func (c *remoteConn) Close() error {
	if c.closed {
		return ErrClosed
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	err := c.dev.Interrupt(ctx)
	if err != nil {
		return fmt.Errorf("Cannot enter command mode: %w", err)
	}
	c.dev.isOnline = false
	c.closed = true

	return nil
}

func (c *remoteConn) WakeupRemote(ctx context.Context) error {
	err := c.dev.Interrupt(ctx)
	if err != nil {
		return fmt.Errorf("Cannot enter command mode: %w", err)
	}
	// Send wakeup command
	c.dev.Exec(ctx, fmt.Sprintf("ATW%d,0", c.remoteAddr))
	// Back to online mode
	return c.dev.sendCmd("ATO")
}
