// Package atm provides an interface to a Benthos acoustic modem
package atm

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	// End of response character
	Eor = ">"
	// End of line character for commands
	Eol          = "\r"
	promptPrefix = "user:"
)

var (
	ErrCmdMode    = errors.New("Modem is not in command mode")
	ErrClosed     = errors.New("Connection is closed")
	ErrBadAddr    = errors.New("Invalid address")
	ErrBadCmd     = errors.New("Invalid command")
	ErrParse      = errors.New("Command parse error")
	ErrNoResponse = errors.New("No response from remote")
)

// TraceFunc is used to log the contents of incoming and outgoing packets
var TraceFunc func(string, ...interface{})

// Trace contains a command sent to the Device along with the
// returned response.
type Trace struct {
	In  string `json:"in"`
	Out string `json:"out"`
}

func validAddr(addr int) bool {
	return (addr >= 0 && addr < 250)
}

// Device represents the modem attached to an io.ReadWriter
type Device struct {
	port     io.ReadWriter
	respBuf  bytes.Buffer
	mu       *sync.Mutex
	isOnline bool
	isAtm88x bool
}

type deviceOptions struct {
	isAtm88x bool
}

type Option struct {
	f func(*deviceOptions)
}

// Set the modem type to ATM-88X.
func ATM88x() Option {
	return Option{f: func(opts *deviceOptions) {
		opts.isAtm88x = true
	}}
}

func NewDevice(port io.ReadWriter, options ...Option) *Device {
	opts := deviceOptions{}
	for _, opt := range options {
		opt.f(&opts)
	}

	return &Device{
		port:     port,
		mu:       &sync.Mutex{},
		isAtm88x: opts.isAtm88x}
}

// Read from the Device until a marker character sequence is detected. All characters up
// to the start of the sequence are returned as a string.
func (d *Device) readUntil(ctx context.Context, marker []byte) (string, error) {

	d.respBuf.Reset()
	tail := -len(marker)
	b := make([]byte, 1)
	for {
		select {
		case <-ctx.Done():
			return d.respBuf.String(), ctx.Err()
		default:
		}

		n, err := d.port.Read(b)
		if errors.Is(err, io.EOF) || errors.Is(err, os.ErrDeadlineExceeded) {
			continue
		}

		if err != nil {
			return d.respBuf.String(), err
		}

		if n == 0 {
			continue
		}

		d.respBuf.Write(b)
		tail++
		if tail < 0 {
			continue
		}

		if bytes.Equal(d.respBuf.Bytes()[tail:], marker) {
			if TraceFunc != nil {
				TraceFunc("RECV: %q", d.respBuf.String())
			}
			return strings.TrimSuffix(d.respBuf.String(), string(marker)), nil
		}
	}

}

func (d *Device) sendCmd(cmd string) error {
	if d.isOnline {
		return ErrCmdMode
	}

	_, err := fmt.Fprintf(d.port, "%s%s", cmd, Eol)
	return err
}

// Online returns true if the Device is in online mode
func (d *Device) Online() bool {
	return d.isOnline
}

// Exec send the supplied command to the Device and returns the response
// along with any error that occurs. The response has the command string and
// any combination of carriage-return and line-feed characters which follow
// the command removed.
func (d *Device) Exec(ctx context.Context, cmd string) (string, error) {
	d.mu.Lock()
	defer d.mu.Unlock()

	var resp string
	err := d.sendCmd(cmd)
	if err != nil {
		return resp, err
	}

	if !d.isAtm88x {
		resp, err = d.readUntil(ctx, []byte(promptPrefix))
		if err != nil {
			return resp, err
		}
		_, err = d.readUntil(ctx, []byte(Eor))

	} else {
		resp, err = d.readUntil(ctx, []byte(Eor))
	}

	if err != nil {
		return resp, err
	}

	if idx := strings.Index(resp, "Invalid Command"); idx >= 0 {
		err = ErrBadCmd
	}

	return strings.TrimLeft(strings.TrimPrefix(resp, cmd), " \t\r\n"), err
}

// SetRegister sets the value of a modem S register
func (d *Device) SetRegister(ctx context.Context, reg, value int) error {
	_, err := d.Exec(ctx, fmt.Sprintf("ATS%02d=%03d", reg, value))
	return err
}

// Register returns the value of a modem S register
func (d *Device) Register(ctx context.Context, reg int) (int, error) {
	resp, err := d.Exec(ctx, fmt.Sprintf("ATS%02d?", reg))
	if err != nil {
		return 0, err
	}
	var value int
	_, err = fmt.Sscanf(resp, "%03d", &value)
	return value, err
}

// RemoteRegisters returns the values of a remote modem's S registers as
// a map. The keys of the map are the register numbers.
func (d *Device) RemoteRegisters(ctx context.Context, addr int) (map[uint]uint, error) {
	if !validAddr(addr) {
		return nil, ErrBadAddr
	}
	resp, err := d.Exec(ctx, fmt.Sprintf("AT$S%d", addr))
	if err != nil {
		return nil, err
	}

	regs := make(map[uint]uint)
	for _, line := range strings.Split(resp, "\n") {
		if line == "" || line[0] != 'S' {
			continue
		}
		for _, pair := range strings.Fields(line) {
			var reg, value uint
			// Each register value is formatted as: SNN=MMM
			_, err := fmt.Sscanf(pair, "S%02d=%03d", &reg, &value)
			if err != nil {
				continue
			}
			regs[reg] = value
		}
	}

	return regs, nil
}

// Batch sends the contents of a command file to the device and returns
// a list of the sent commands and responses along with any error that
// occurs.
func (d *Device) Batch(ctx context.Context, cmdfile io.Reader) ([]Trace, error) {
	log := make([]Trace, 0)
	scanner := bufio.NewScanner(cmdfile)
	line := int(0)
	var (
		reply string
		err   error
	)

	for scanner.Scan() {
		line++
		b := scanner.Bytes()
		if len(b) == 0 || b[0] == '#' {
			// Skip empty lines and comment lines
			continue
		}

		cmd := strings.TrimRight(scanner.Text(), " \t\r\n")

		switch strings.ToLower(cmd) {
		case "+++":
			err = d.Interrupt(ctx)
		case "@opmode=online", "@opmode=1", "ats15=1":
			d.mu.Lock()
			d.sendCmd(cmd)
			d.mu.Unlock()
			time.Sleep(time.Second)
			log = append(log, Trace{In: cmd})
		default:
			reply, err = d.Exec(ctx, cmd)
			log = append(log, Trace{In: cmd, Out: reply})
		}

		if err != nil {
			return log, fmt.Errorf("Error on line %d: %w", line, err)
		}
	}

	return log, scanner.Err()
}

// Interrupt forces the Device into command mode.
func (d *Device) Interrupt(ctx context.Context) error {
	d.mu.Lock()
	defer d.mu.Unlock()
	// There must be no characters sent to the modem for at least one
	// second before the interrupt sequence is sent.
	time.Sleep(time.Second)
	d.port.Write([]byte("+++"))
	_, err := d.readUntil(ctx, []byte(Eor))
	if err == nil {
		d.isOnline = false
	}

	return err
}

// Range returns the range in meters to the remote modem addr
func (d *Device) Range(ctx context.Context, addr int) (float64, error) {
	if !validAddr(addr) {
		return 0, ErrBadAddr
	}

	resp, err := d.Exec(ctx, fmt.Sprintf("ATR%d", addr))
	if err != nil {
		return 0, err
	}

	var val float64
	if idx := strings.Index(resp, ":"); idx >= 0 {
		_, err = fmt.Sscanf(resp[idx+1:], "%f m", &val)
	} else if idx := strings.Index(resp, "="); idx >= 0 {
		_, err = fmt.Sscanf(resp[idx+1:], "%f Meters", &val)
	}

	return val, err
}

// Lowpower puts the modem into low power mode
func (d *Device) Lowpower(ctx context.Context) error {
	d.mu.Lock()
	defer d.mu.Unlock()

	d.sendCmd("ATL")
	_, err := d.readUntil(ctx, []byte("Lowpower\r\n"))
	return err
}

func (d *Device) Listen(ctx context.Context) (Conn, error) {
	for _, cmd := range []string{"ATS13=00", "AT&W", "ATL"} {
		_, err := d.Exec(ctx, cmd)
		if err != nil {
			return nil, err
		}
		time.Sleep(time.Millisecond * 20)
	}
	d.isOnline = true
	return &remoteConn{dev: d, remoteAddr: -1}, nil
}

func (d *Device) GoOnline(ctx context.Context) (Conn, error) {
	d.isOnline = true
	return &remoteConn{dev: d, remoteAddr: -1}, nil
}

// Accept waits for a connection from a remote modem
func (d *Device) Accept(ctx context.Context) (Conn, error) {
	// Increase verbosity to get the "wakeup" message
	err := d.SetRegister(ctx, 13, 3)
	if err != nil {
		return nil, err
	}

	// Enter low power mode
	err = d.Lowpower(ctx)
	if err != nil {
		return nil, err
	}

	var addr int
	for {
		d.mu.Lock()
		s, err := d.readUntil(ctx, []byte("\n"))
		d.mu.Unlock()

		if strings.Trim(s, " \t\r\n") == "Acoustic Wakeup" {
			d.isOnline = true
			// Temporarily set the modem to command mode and reduce
			// verbosity level
			if d.Interrupt(ctx) == nil {
				d.SetRegister(ctx, 13, 0)
				// Read remote address from S register
				addr, _ = d.Register(ctx, 14)
				d.mu.Lock()
				d.sendCmd("ATO")
				d.mu.Unlock()
			}
			break
		}

		if err != io.EOF {
			return nil, err
		}

	}

	return &remoteConn{dev: d, remoteAddr: addr}, nil
}

// AcceptFrom place the local modem online with the remote modem
// at address addr (passive connect).
func (d *Device) AcceptFrom(ctx context.Context, addr int) (Conn, error) {
	if !validAddr(addr) {
		return nil, ErrBadAddr
	}

	err := d.SetRegister(ctx, 14, addr)
	if err != nil {
		return nil, err
	}
	d.SetRegister(ctx, 13, 0)
	// TODO: verify response from ATO command when @Verbose=0
	d.mu.Lock()
	defer d.mu.Unlock()
	d.sendCmd("ATO")
	d.isOnline = true

	return &remoteConn{dev: d, remoteAddr: addr}, nil
}

// Connect initiates a connection to the remote modem at address addr.
func (d *Device) Connect(ctx context.Context, addr int) (Conn, error) {
	if !validAddr(addr) {
		return nil, ErrBadAddr
	}

	d.SetRegister(ctx, 13, 1)
	d.mu.Lock()
	defer d.mu.Unlock()

	err := d.sendCmd(fmt.Sprintf("ATD%d", addr))
	if err != nil {
		return nil, err
	}

	var baud int
	for {
		s, err := d.readUntil(ctx, []byte("\n"))
		if err != nil {
			return nil, err
		}

		if strings.Trim(s, " \r\n") == "Response Not Received" {
			return nil, ErrNoResponse
		}

		if strings.HasPrefix(s, "CONNECT") {
			fmt.Sscanf(s, "CONNECT %05d", &baud)
			d.isOnline = true
			break
		}
	}

	return &remoteConn{dev: d, remoteAddr: addr, baud: baud}, nil
}
