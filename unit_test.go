package atm

import (
	"bytes"
	"context"
	"testing"
	"time"
)

// Test parsing the modem responses
func TestRespParse(t *testing.T) {
	table := []struct {
		in, out, marker string
	}{
		{
			in:     "\r\n\r\n042\r\nuser:1>",
			out:    "\r\n\r\n042\r\nuser:1",
			marker: ">",
		},
		{
			in:     "\r\n\r\nuser:2>Lowpower\r\n",
			out:    "\r\n\r\nuser:2>",
			marker: "Lowpower\r\n",
		},
	}

	var b bytes.Buffer
	dev := NewDevice(&b)

	for _, e := range table {
		b.Write([]byte(e.in))
		resp, err := dev.readUntil(context.Background(), []byte(e.marker))
		if err != nil {
			t.Fatal(err)
		}
		if resp != e.out {
			t.Errorf("Bad response; expected %q, got %q", e.out, resp)
		}
	}
}

func TestExec(t *testing.T) {
	table := []struct {
		in, out string
		err     error
		opts    []Option
	}{
		{
			in:  "\r\n\r\n042\r\nuser:1>",
			out: "042\r\n",
		},
		{
			in:  "AT\r\n\r\n042\r\nuser:1>",
			out: "042\r\n",
		},
		{
			in:  "AT\r\nInvalid Command\r\n\r\nuser:1>",
			out: "Invalid Command\r\n\r\n",
			err: ErrBadCmd,
		},
		{
			in:   "AT\r\n\r\n042\r\n>",
			out:  "042\r\n",
			opts: []Option{ATM88x()},
		},
	}

	var b bytes.Buffer
	for _, e := range table {
		dev := NewDevice(&b, e.opts...)
		b.Write([]byte(e.in))
		resp, err := dev.Exec(context.Background(), "AT")
		if err != e.err {
			t.Fatalf("Unexpected error value: %v", err)
		}
		if resp != e.out {
			t.Errorf("Bad response; expected %q, got %q", e.out, resp)
		}
		// Sent command should be at the end of the buffer
		sent := b.String()
		if sent != "AT\r" {
			t.Errorf("Unexpected buffer contents: %q", sent)
		}
		b.Reset()
	}
}

func TestRange(t *testing.T) {
	in := "\r\nRange 1 to 2 : 4500.0 m\r\nuser:351>"
	expected := float64(4500)

	var b bytes.Buffer
	dev := NewDevice(&b)
	b.Write([]byte(in))
	val, err := dev.Range(context.Background(), 2)
	if err != nil {
		t.Fatal(err)
	}

	if val != expected {
		t.Errorf("Bad range value; expected %v, got %v", expected, val)
	}
}

func TestOldRange(t *testing.T) {
	in := "\r\nRange=004.7 Meters\r\n>"
	expected := float64(4.7)

	var b bytes.Buffer
	dev := NewDevice(&b, ATM88x())
	b.Write([]byte(in))
	val, err := dev.Range(context.Background(), 2)
	if err != nil {
		t.Fatal(err)
	}

	if val != expected {
		t.Errorf("Bad range value; expected %v, got %v", expected, val)
	}
}

func TestRemoteReg(t *testing.T) {
	in := []string{
		"\r\nRemote Sregisters\r\n",
		"S00=000  S01=000  S02=000  S03=002  S04=008  S05=000  S06=007\r\n",
		"S07=015  S08=010  S09=000  S10=255  S11=000  S12=000  S13=001\r\n",
		"S14=002  S15=001  S16=001  S17=001  S18=001  S19=000  S20=000\r\n",
		"user:6>",
	}
	var b bytes.Buffer
	dev := NewDevice(&b)
	for _, e := range in {
		b.Write([]byte(e))
	}

	regs, err := dev.RemoteRegisters(context.Background(), 2)
	if err != nil {
		t.Fatal(err)
	}

	expected := map[uint]uint{
		0:  0,
		6:  7,
		10: 255,
		14: 2,
		20: 0,
	}

	if len(regs) != 21 {
		t.Errorf("Bad map size; expected 21, got %d", len(regs))
	}

	for k, v := range expected {
		if regs[k] != v {
			t.Errorf("Bad value for register %q; expected %d, got %d",
				k, v, regs[k])
		}
	}
}

func TestRegRead(t *testing.T) {
	table := []struct {
		in  string
		out int
	}{
		{
			in:  "\r\n\r\n042\r\nuser:1>",
			out: 42,
		},
	}

	var b bytes.Buffer
	dev := NewDevice(&b)
	for _, e := range table {
		b.Write([]byte(e.in))
		val, err := dev.Register(context.Background(), 3)
		if err != nil {
			t.Fatal(err)
		}
		if val != e.out {
			t.Errorf("Bad response; expected %v, got %v", e.out, val)
		}
		// Sent command should be at the end of the buffer
		sent := b.String()
		if sent != "ATS03?\r" {
			t.Errorf("Unexpected buffer contents: %q", sent)
		}
		b.Reset()
	}
}

const (
	RESP = "\r\nOK\r\nuser:1>\r\n\r\nuser:1>Lowpower\r\nAcoustic Wakeup\r\nuser:1>\r\nOK\r\nuser:1>\r\n\r\n003\r\nuser:1>"
	SENT = "ATS13=003\rATL\r+++ATS13=000\rATS14?\rATO\r"
)

func TestAccept(t *testing.T) {
	var b bytes.Buffer
	dev := NewDevice(&b)
	// Load the expected modem responses
	b.Write([]byte(RESP))
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	conn, err := dev.Accept(ctx)
	if err != nil {
		t.Fatal(err)
	}

	if conn.RemoteAddr() != 3 {
		t.Errorf("Wrong remote address; expected 3, got %d", conn.RemoteAddr())
	}

	sent := b.String()
	if sent != SENT {
		t.Errorf("Unexpected buffer contents: %q", sent)
	}
}
